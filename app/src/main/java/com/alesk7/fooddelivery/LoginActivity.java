package com.alesk7.fooddelivery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.alesk7.fooddelivery.fragments.LoginFragment;
import com.alesk7.fooddelivery.fragments.RegistrationFragment;

public class LoginActivity extends AppCompatActivity implements LoginFragment.OnRegistrationButtonClicked {
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Object obj = getLastCustomNonConfigurationInstance();
        if(obj != null) {
            fragment = (Fragment) obj;
        }else{
            fragment = new LoginFragment();
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.auth_container, fragment).commit();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return getSupportFragmentManager().findFragmentById(R.id.auth_container);
    }

    @Override
    public void buttonClicked() {
        fragment = new RegistrationFragment();
        getSupportFragmentManager().beginTransaction().addToBackStack(null)
                .replace(R.id.auth_container, fragment).commit();
    }
}