package com.alesk7.fooddelivery.views;

import com.alesk7.fooddelivery.entities.Address;

import io.realm.RealmResults;

/**
 * Created by Acer on 11-Dec-17.
 */

public interface OrderConfirmationView {
    void setCost(float cost);
    void setAuthVisible(boolean flag);
    void setDiscountPercentageVisible(boolean flag);
    void setDiscountPercentage(float percentage);
    void setAvailableDiscount(double discount);
    Address getAddress();
    String getName();
    String getPhoneNumber();
    void setName(String name);
    void setAddressesList(RealmResults<Address> data);
    void showLoadingView();
    void hideLoadingView();
    void showSuccessSendingView();
    void showClientError();
    void showFillFieldsMessage();
}
