package com.alesk7.fooddelivery.views.callbacks;

/**
 * Created by Acer on 19-Nov-17.
 */

public interface FoodImageLoadedCallback {
    void notifyImageLoaded();
}
