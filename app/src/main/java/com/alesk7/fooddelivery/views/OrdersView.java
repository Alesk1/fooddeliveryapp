package com.alesk7.fooddelivery.views;

import com.alesk7.fooddelivery.entities.Order;

import java.util.List;

/**
 * Created by Acer on 01-Nov-17.
 */

public interface OrdersView {
    void ordersDataLoaded(List<Order> data);
    void showLoadingIndicator();
    void hideLoadingIndicator();
    void showAuthorization();
    void hideAuthorization();
}
