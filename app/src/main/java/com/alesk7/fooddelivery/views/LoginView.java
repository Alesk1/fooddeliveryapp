package com.alesk7.fooddelivery.views;

/**
 * Created by Acer on 27-Sep-17.
 */

public interface LoginView {
    void showLoadingIndicator();
    void hideLoadingIndicator();
    void showIncorrectDataMessage();
    void showErrorMessage();
    String getEmail();
    String getPassword();
    void hideLogInScreen();
}
