package com.alesk7.fooddelivery.views;

import com.alesk7.fooddelivery.entities.Food;

import java.util.List;

/**
 * Created by Acer on 14-Oct-17.
 */

public interface CartView {
    void setData(List<Food> data);
    void setOrderButtonVisibility(boolean visibility);
    void setSum(double sum);
    void showEmptyCartView();
    void hideEmptyCartView();
}
