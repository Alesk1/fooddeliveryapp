package com.alesk7.fooddelivery.views;

/**
 * Created by Acer on 27-Sep-17.
 */

public interface RegisterView {
    String getName();
    String getLastName();
    String getEmail();
    String getPassword();
    void hideRegisterScreen();
    void showLoadingIndicator();
    void hideLoadingIndicator();
    void showIncorrectDataMessage();
    void showErrorMessage();
    void showEmailNotUnique();
}
