package com.alesk7.fooddelivery.views;

import com.alesk7.fooddelivery.entities.Food;

import java.util.List;

/**
 * Created by Acer on 22-Nov-17.
 */

public interface FoodView {
    void showNoConnectionView();
    void hideNoConnectionView();
    void showErrorView();
    void hideErrorView();
    void showLoadingView();
    void hideLoadingView();
    void setData(List<Food> food);
}
