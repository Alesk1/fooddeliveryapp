package com.alesk7.fooddelivery.views;

import com.alesk7.fooddelivery.entities.Address;

import io.realm.RealmResults;

/**
 * Created by Acer on 03-Nov-17.
 */

public interface ClientView {
    void setName(String name);
    void setEmail(String email);
    void setBonusCash(float cash);
    void showAuthorizationView();
    void hideAuthorizationView();
    void setAddresses(RealmResults<Address> data);
    void showLoadingIndicator();
    void hideLoadingIndicator();
}
