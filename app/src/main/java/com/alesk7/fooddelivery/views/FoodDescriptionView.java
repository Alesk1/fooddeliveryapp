package com.alesk7.fooddelivery.views;

import android.graphics.Bitmap;

/**
 * Created by Acer on 06-Oct-17.
 */

public interface FoodDescriptionView {
    void setFoodName(String foodName);
    void setFoodSize(String foodSize);
    void setFoodWeight(int foodWeight);
    void setCookingTime(int cookingTime);
    void setFoodDescription(String foodDescription);
    void setFoodPicture(Bitmap foodPicture);
    void setPrice(float price);
    void showAddedToCartMessage();
    int getQuantity();
    void setQuantuty(int quantuty);
}
