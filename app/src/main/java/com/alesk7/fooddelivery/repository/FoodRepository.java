package com.alesk7.fooddelivery.repository;

import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.entities.FoodType;
import com.alesk7.fooddelivery.networking.RetrofitFactory;
import com.alesk7.fooddelivery.networking.services.FoodService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Acer on 16-Dec-17.
 */

public class FoodRepository {
    private FoodService service;

    public FoodRepository(){
        service = RetrofitFactory.build().create(FoodService.class);
    }

    public Observable<List<Food>> get(FoodType type){
        return service.getFood(type.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
