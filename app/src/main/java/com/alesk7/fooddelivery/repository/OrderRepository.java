package com.alesk7.fooddelivery.repository;

import com.alesk7.fooddelivery.entities.Order;
import com.alesk7.fooddelivery.networking.RetrofitFactory;
import com.alesk7.fooddelivery.networking.services.OrderService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by Acer on 19-Dec-17.
 */

public class OrderRepository {
    private OrderService service;

    public OrderRepository(){
        service = RetrofitFactory.build().create(OrderService.class);
    }

    public Observable<Response<Void>> create(Order order){
        return service.addOrder(order)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Order>> getAll(int clientId){
        return service.getOrders(clientId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
