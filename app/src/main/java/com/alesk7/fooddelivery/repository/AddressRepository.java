package com.alesk7.fooddelivery.repository;

import android.util.Log;

import com.alesk7.fooddelivery.entities.Address;
import com.alesk7.fooddelivery.networking.RetrofitFactory;
import com.alesk7.fooddelivery.networking.services.AddressService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Response;

/**
 * Created by Acer on 17-Dec-17.
 */

public class AddressRepository {
    private AddressService service;
    private Realm realm;

    public AddressRepository(){
        service = RetrofitFactory.build().create(AddressService.class);
        realm = Realm.getDefaultInstance();
    }

    public RealmResults<Address> getAllClientAddresses(int id){
        service.getClientAddresses(Integer.toString(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(addresses1 -> { clearLocal(); addAllLocal(addresses1); },
                        throwable -> Log.e(throwable.getLocalizedMessage(), throwable.getMessage()));

        return realm.where(Address.class).findAll();
    }

    public Observable<Response<Integer>> create(Address address){
        return service.addAddress(address.getClientId(), address.getAddress())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterTerminate(() -> createLocal(address));
    }

    public Observable<Response<Void>> update(Address address, String newAddress){
        return service.updateAddress(address.getId(), address.getClientId(), newAddress)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> updateLocal(address, newAddress));
    }

    public Observable<Response<Void>> delete(Address address){
        return service.deleteAddress(address.getId(), address.getClientId(), address.getAddress())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> deleteLocal(address));
    }

    private void createLocal(Address address){
        realm.beginTransaction();
        Address a = realm.createObject(Address.class);
        a.setId(address.getId());
        a.setClientId(address.getClientId());
        a.setAddress(address.getAddress());
        realm.commitTransaction();
    }

    private void updateLocal(Address address, String newAddress){
        realm.executeTransaction(realm -> realm.where(Address.class)
                .equalTo("id", address.getId())
                .findFirst()
                .setAddress(newAddress));
    }

    private void deleteLocal(Address address){
        realm.executeTransaction(realm -> realm.where(Address.class)
                .equalTo("id", address.getId())
                .findFirst()
                .deleteFromRealm());
    }

    private void addAllLocal(List<Address> addresses){
        realm.beginTransaction();
        realm.copyToRealm(addresses);
        realm.commitTransaction();
    }

    public void clearLocal(){
        try {
            realm.beginTransaction();
            realm.where(Address.class).findAll().deleteAllFromRealm();
            realm.commitTransaction();
        }catch (NullPointerException e){
            Log.e("EMPTY DB ADDRESS", "No registered addresses in realm!");
        }
    }

}
