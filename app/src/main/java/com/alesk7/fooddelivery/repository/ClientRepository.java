package com.alesk7.fooddelivery.repository;

import android.util.Log;

import com.alesk7.fooddelivery.entities.Client;
import com.alesk7.fooddelivery.networking.RetrofitFactory;
import com.alesk7.fooddelivery.networking.services.ClientService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import retrofit2.Response;

/**
 * Created by Acer on 16-Dec-17.
 */

public class ClientRepository {
    private ClientService service;
    private Realm realm;

    public ClientRepository(){
        service = RetrofitFactory.build().create(ClientService.class);
        realm = Realm.getDefaultInstance();
    }

    public static Client get(){
        return Realm.getDefaultInstance().where(Client.class).findFirst();
    }

    public Observable<List<Client>> getAllClients(){
        return service.getClients()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<Integer>> create(Client client){
        return service.addClient(client.getFirstName(), client.getLastName(), client.getEmail(), client.getPassword(), client.getBonusCash())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    }

    public void createLocal(Client client){
        realm.beginTransaction();
        realm.copyToRealm(client);
        realm.commitTransaction();
    }

    public Observable<Response<Void>> update(Client newObj){
        return service.updateClient(newObj.getId(), newObj.getFirstName(), newObj.getLastName(), newObj.getEmail(), newObj.getPassword(), newObj.getBonusCash())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> updateLocal(newObj));
    }

    private void updateLocal(Client client){
        realm.executeTransaction(realm -> {
            Client client1 = realm.where(Client.class).findFirst();
            client1.setFirstName(client.getFirstName());
            client1.setLastName(client.getLastName());
            client1.setEmail(client.getEmail());
            client1.setPassword(client.getPassword());
            client1.setBonusCash(client.getBonusCash());
        });
    }

    public void clearLocal(){
        try {
            realm.beginTransaction();
            realm.where(Client.class).findAll().deleteAllFromRealm();
            realm.commitTransaction();
        }catch (NullPointerException e){
            Log.e("EMPTY_DB_CLIENTS", "No registered clients in realm!");
        }
    }
}
