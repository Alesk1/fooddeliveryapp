package com.alesk7.fooddelivery.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alesk7.fooddelivery.MainActivity;
import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.entities.Address;
import com.alesk7.fooddelivery.presenters.ClientPresenter;
import com.alesk7.fooddelivery.views.ClientView;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class ClientFragment extends Fragment implements ClientView {
    private ClientListener mListener;
    private ClientPresenter presenter;
    private AddressesAdapter addressesAdapter;
    private CardView mCredentials;
    private CardView mAddresses;
    private View mAuthorization;
    private TextView mName;
    private TextView mEmail;
    private TextView mBonusCash;
    private LinearLayout mProgLogOutLayout;
    private ProgressBar mProgressBar;


    @Override
    public void setName(String name) {
        mName.setText(name);
    }

    @Override
    public void setEmail(String email) {
        mEmail.setText(email);
    }

    @Override
    public void setBonusCash(float cash) {
        mBonusCash.setText(String.format(getString(R.string.price), cash));
    }

    @Override
    public void showAuthorizationView() {
        mProgLogOutLayout.setVisibility(View.GONE);
        mCredentials.setVisibility(View.GONE);
        mAddresses.setVisibility(View.GONE);

        mAuthorization.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideAuthorizationView() {
        mProgLogOutLayout.setVisibility(View.VISIBLE);
        mCredentials.setVisibility(View.VISIBLE);
        mAddresses.setVisibility(View.VISIBLE);

        mAuthorization.setVisibility(View.GONE);
    }

    @Override
    public void setAddresses(RealmResults<Address> data) {
        addressesAdapter.setData(data);
    }

    @Override
    public void showLoadingIndicator() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingIndicator() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    private void showLogOutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Выход из аккаунта")
                .setMessage("Вы действительно хотите выйти из аккаунта?")
                .setCancelable(true)
                .setNegativeButton("Нет", ((dialog, i) -> dialog.cancel()))
                .setPositiveButton("Да", (dialog, id) -> presenter.logOut());
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*public static void showAddressDialog(Address address, boolean add) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        EditText mAddressText = new EditText(new ContextThemeWrapper(getContext(), R.style.EditTextTheme));
        if(!add) mAddressText.setText(address.getAddress());
        builder.setTitle(add ? "Добавить адрес" : "Редактировать адрес")
                .setView(mAddressText)
                .setCancelable(true)
                .setNegativeButton("Отмена", ((dialog, i) -> dialog.cancel()))
                .setPositiveButton(add ? "Добавить" : "Сохранить",
                        add ? (dialog, id) -> presenter.addAddress(mAddressText.getText().toString())
                            : (dialog, id) -> presenter.editAddress(address, mAddressText.getText().toString()));
        AlertDialog alert = builder.create();
        alert.show();
    }*/

    public void showDeleteAddressDialog(Address address) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Удалить адрес")
                .setMessage("Вы действительно хотите удалить адрес?")
                .setCancelable(true)
                .setNegativeButton("Нет", ((dialog, i) -> dialog.cancel()))
                .setPositiveButton("Да", (dialog, id) -> presenter.deleteAddress(address));
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListener = (ClientListener) getContext();
        presenter = new ClientPresenter();
        presenter.bindView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client, container, false);

        mName = view.findViewById(R.id.name);
        mEmail = view.findViewById(R.id.email);
        mBonusCash = view.findViewById(R.id.bonus_cash);
        mCredentials = view.findViewById(R.id.credentials_card);
        mAddresses = view.findViewById(R.id.addresses_card);
        mAuthorization = view.findViewById(R.id.authorization);
        view.findViewById(R.id.auth_btn).setOnClickListener((v) -> mListener.onAuthorizationClick());
        mProgressBar = view.findViewById(R.id.progress_bar);
        mProgLogOutLayout = view.findViewById(R.id.prog_logout_layout);
        Button mAddAddressBtn = view.findViewById(R.id.btn_add_address);
        RecyclerView recyclerView = view.findViewById(R.id.addresses_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);

        addressesAdapter = new AddressesAdapter();
        recyclerView.setAdapter(addressesAdapter);

        view.findViewById(R.id.btn_log_out).setOnClickListener((v) -> showLogOutDialog());
        mAddAddressBtn.setOnClickListener((v) -> ((MainActivity)getActivity()).showAddressDialog(null, true,
                address -> presenter.addAddress(address), null));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    class AddressesAdapter extends RecyclerView.Adapter<AddressesAdapter.ViewHolder> implements RealmChangeListener{
        RealmResults<Address> data;

        void setData(RealmResults<Address> data){
            this.data = data;
            data.addChangeListener(addresses -> notifyDataSetChanged());
        }

        @Override
        public void onChange(Object o) {
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(ClientFragment.this.getContext())
                    .inflate(R.layout.address_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.mItem = data.get(position);
            holder.address.setText(holder.mItem.getAddress());
            holder.mEdit.setOnClickListener((v) -> ((MainActivity)getActivity()).showAddressDialog(holder.mItem, false,
                    null, (oldAddress, newAddress) -> presenter.editAddress(oldAddress, newAddress)));
            holder.mDelete.setOnClickListener((v) -> showDeleteAddressDialog(holder.mItem));
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            Address mItem;
            TextView address;
            ImageButton mEdit;
            ImageButton mDelete;

            ViewHolder(View itemView) {
                super(itemView);
                address = itemView.findViewById(R.id.address);
                mEdit = itemView.findViewById(R.id.btn_edit);
                mDelete = itemView.findViewById(R.id.btn_delete);
            }
        }
    }

    public interface ClientListener{
        void onAuthorizationClick();
    }
}
