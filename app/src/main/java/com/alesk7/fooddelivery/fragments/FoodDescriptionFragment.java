package com.alesk7.fooddelivery.fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.presenters.FoodDescriptionPresenter;
import com.alesk7.fooddelivery.views.FoodDescriptionView;

import pl.polak.clicknumberpicker.ClickNumberPickerView;

public class FoodDescriptionFragment extends Fragment implements FoodDescriptionView {
    private FoodDescriptionPresenter presenter;
    private CartClickListener listener;
    private TextView foodName;
    private TextView foodSize;
    private TextView foodWeight;
    private TextView cookingTime;
    private TextView foodDescription;
    private ImageView foodPicture;
    private ClickNumberPickerView countPicker;
    private Button addToCartButton;

    public void setFoodName(String foodName) {
        this.foodName.setText(foodName);
    }

    public void setFoodSize(String foodSize) {
        this.foodSize.setText(foodSize);
    }

    public void setFoodWeight(int foodWeight) {
        this.foodWeight.setText(String.format(getString(R.string.food_weight), foodWeight));
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime.setText(String.format(getString(R.string.cooking_time), cookingTime));
    }

    public void setFoodDescription(String foodDescription) {
        this.foodDescription.setText(foodDescription);
    }

    public void setFoodPicture(Bitmap foodPicture) {
        this.foodPicture.setImageBitmap(foodPicture);
    }

    public void setPrice(float price){
        this.addToCartButton.setText(String.format(getString(R.string.price_on_button), price));
    }

    @Override
    public int getQuantity() {
        return (int)countPicker.getValue();
    }

    @Override
    public void setQuantuty(int quantuty) {
        this.countPicker.setPickerValue(quantuty);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void showAddedToCartMessage(){
        Snackbar snackbar = Snackbar.make(getView(), "Добавлено в корзину", Snackbar.LENGTH_SHORT);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) snackbar.getView().getLayoutParams();
        int bottomMargin = getActivity().findViewById(R.id.bottomNavigationView).getHeight();
        layoutParams.setMargins(0, 0, 0, bottomMargin);
        snackbar.getView().setElevation(0.0f);
        snackbar.getView().setLayoutParams(layoutParams);
        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        snackbar.setAction("Посмотреть", v -> listener.onCartClick());
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listener = (CartClickListener) getActivity();
        presenter = new FoodDescriptionPresenter();
        presenter.bindView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_food_description, container, false);

        foodName = view.findViewById(R.id.food_name);
        foodSize = view.findViewById(R.id.food_size);
        foodWeight = view.findViewById(R.id.food_weight);
        cookingTime = view.findViewById(R.id.cooking_time);
        foodDescription = view.findViewById(R.id.ingredients);
        foodPicture = view.findViewById(R.id.food_picture);
        addToCartButton = view.findViewById(R.id.add_to_cart_button);
        countPicker = view.findViewById(R.id.count);

        presenter.onCreate((Food)getArguments().getSerializable(getString(R.string.food_item)));

        countPicker.setClickNumberPickerListener((previousValue, currentValue, pickerClickType) -> presenter.onChangeCount((int)currentValue));

        addToCartButton.setOnClickListener(view1 -> presenter.onBtnCartClick());

        return view;
    }

    public interface CartClickListener{
        void onCartClick();
    }
}
