package com.alesk7.fooddelivery.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.adapters.OrdersAdapter;
import com.alesk7.fooddelivery.entities.Order;
import com.alesk7.fooddelivery.presenters.OrdersPresenter;
import com.alesk7.fooddelivery.views.OrdersView;

import java.util.List;

public class OrdersFragment extends Fragment implements OrdersView{
    private OrdersListener mListener;
    private OrdersPresenter presenter;
    private OrdersAdapter ordersAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View mAuthorization;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListener = (OrdersListener) getActivity();
        presenter = new OrdersPresenter();
        presenter.bindView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_oders, container, false);

        mAuthorization = view.findViewById(R.id.authorization);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        ordersAdapter = new OrdersAdapter(this.getContext());
        recyclerView.setAdapter(ordersAdapter);

        swipeRefreshLayout.setOnRefreshListener(() -> presenter.loadOrders());
        view.findViewById(R.id.auth_btn).setOnClickListener((v) -> mListener.onAuthorizationClick());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    public interface OrdersListener{
        void onAuthorizationClick();
    }

    @Override
    public void ordersDataLoaded(List<Order> data){ ordersAdapter.setData(data); }
    @Override
    public void showLoadingIndicator() { swipeRefreshLayout.setRefreshing(true); }
    @Override
    public void hideLoadingIndicator() { swipeRefreshLayout.setRefreshing(false); }

    @Override
    public void showAuthorization() {
        mAuthorization.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideAuthorization() {
        mAuthorization.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }
}
