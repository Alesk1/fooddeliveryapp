package com.alesk7.fooddelivery.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alesk7.fooddelivery.MainActivity;
import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.entities.Address;
import com.alesk7.fooddelivery.presenters.OrderConfirmationPresenter;
import com.alesk7.fooddelivery.views.OrderConfirmationView;

import io.realm.RealmResults;


public class OrderConfirmationFragment extends Fragment implements OrderConfirmationView{
    private OrderConfirmationListener mListener;
    private OrderConfirmationPresenter presenter;
    private View mAuthorization;
    private ScrollView mCredentials;
    private TextView mCost;
    private EditText mName;
    private EditText mPhone;
    private CheckBox mUseDiscount;
    private TextView mAvailableDiscount;
    private TextView mPercentage;
    private Button mSendOrder;
    private Spinner mAddresses;
    private ArrayAdapter<Address> spinnerAdapter;
    private ProgressDialog progressDialog;

    @Override
    public void setCost(float cost) {
        mCost.setText(String.format(getString(R.string.price), cost));
    }

    @Override
    public void setAuthVisible(boolean flag) {
        if(flag){
            mAuthorization.setVisibility(View.VISIBLE);
            mCredentials.setVisibility(View.GONE);
        } else {
            mAuthorization.setVisibility(View.GONE);
            mCredentials.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setDiscountPercentageVisible(boolean flag) {
        if(flag) mPercentage.setVisibility(View.VISIBLE);
        else mPercentage.setVisibility(View.GONE);
    }

    @Override
    public void setDiscountPercentage(float percentage) {
        mPercentage.setText(String.format(getString(R.string.discount_percentage), percentage));
    }

    @Override
    public void setAvailableDiscount(double discount) {
        mAvailableDiscount.setText(String.format(getString(R.string.available_discount), discount));
    }

    @Override
    public Address getAddress() {
        return (Address) mAddresses.getSelectedItem();
    }

    @Override
    public String getName() {
        return mName.getText().toString();
    }

    @Override
    public String getPhoneNumber() {
        return mPhone.getText().toString();
    }

    @Override
    public void setName(String name) {
        mName.setText(name);
    }

    @Override
    public void setAddressesList(RealmResults<Address> data) {
        spinnerAdapter.clear();
        spinnerAdapter.addAll(data);
    }

    @Override
    public void showLoadingView() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Подождите");
        progressDialog.setMessage("Идет отправка данных на сервер");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoadingView() {
        progressDialog.dismiss();
    }

    @Override
    public void showSuccessSendingView() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Спасибо!")
                .setMessage("Ваш заказ зарегистрирован.")
                .setCancelable(true)
                .setPositiveButton("Ок", (dialog, i) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void showClientError() {
        Toast.makeText(getContext(), getString(R.string.client_error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showFillFieldsMessage() {
        Toast.makeText(getContext(), "Заполните все поля!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListener = (OrderConfirmationListener) getActivity();
        presenter = new OrderConfirmationPresenter();
        presenter.bindView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_confirmation, container, false);
        mAuthorization = view.findViewById(R.id.authorization);
        mCredentials = view.findViewById(R.id.credentials);
        mCost = view.findViewById(R.id.cost);
        mName = view.findViewById(R.id.name);
        mPhone = view.findViewById(R.id.phone_number);
        mAddresses = view.findViewById(R.id.addresses);
        mUseDiscount = view.findViewById(R.id.use_discount);
        mAvailableDiscount = view.findViewById(R.id.available_discount);
        mPercentage = view.findViewById(R.id.percentage);
        mSendOrder = view.findViewById(R.id.btn_send_order);

        view.findViewById(R.id.btn_add_address).setOnClickListener((v) -> ((MainActivity)getActivity()).showAddressDialog(null,
                true, address -> presenter.addAddress(address), null));
        view.findViewById(R.id.auth_btn).setOnClickListener((v) -> mListener.onAuthorizationClick());
        mUseDiscount.setOnCheckedChangeListener((compoundButton, b) -> presenter.onUseDiscountChecked(mUseDiscount.isChecked()));
        mSendOrder.setOnClickListener((v) -> presenter.trySendOrder());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        spinnerAdapter = new ArrayAdapter<Address>(getContext(), R.layout.support_simple_spinner_dropdown_item);
        presenter.onResume();
        mAddresses.setAdapter(spinnerAdapter);
    }

    public interface OrderConfirmationListener{
        void onAuthorizationClick();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener = null;
    }
}
