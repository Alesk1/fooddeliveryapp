package com.alesk7.fooddelivery.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.presenters.LoginPreseter;
import com.alesk7.fooddelivery.views.LoginView;

public class LoginFragment extends Fragment implements LoginView {
    private OnRegistrationButtonClicked onRegistrationButtonClicked;
    private LoginPreseter loginPreseter;
    private ProgressDialog progressDialog;
    private EditText mEmail;
    private EditText mPassword;

    public LoginFragment() {}

    @Override
    public void showLoadingIndicator() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Загрузка");
        progressDialog.setMessage("Идет авторизация");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoadingIndicator() {
        progressDialog.dismiss();
    }

    @Override
    public void showIncorrectDataMessage() {
        Toast.makeText(getContext(), "Неверный логин или пароль!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(getContext(), "Ошибка", Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getEmail() {
        return mEmail.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPassword.getText().toString();
    }

    @Override
    public void hideLogInScreen() {
        getActivity().finish();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginPreseter = new LoginPreseter();
        loginPreseter.bindView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        view.findViewById(R.id.log_in_button).setOnClickListener(view12 -> loginPreseter.tryLogIn());
        view.findViewById(R.id.registration_button).setOnClickListener(view1 -> onRegistrationButtonClicked.buttonClicked());

        mEmail = view.findViewById(R.id.email);
        mPassword = view.findViewById(R.id.password);

        return view;
    }

    public interface OnRegistrationButtonClicked{
        void buttonClicked();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onRegistrationButtonClicked = (OnRegistrationButtonClicked) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnRegistrationButtonClicked");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onRegistrationButtonClicked = null;
    }
}