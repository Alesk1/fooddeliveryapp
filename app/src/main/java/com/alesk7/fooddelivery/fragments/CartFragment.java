package com.alesk7.fooddelivery.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.adapters.CartAdapter;
import com.alesk7.fooddelivery.adapters.CartItemTouchHelperAdapter;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.presenters.CartPresenter;
import com.alesk7.fooddelivery.views.CartView;

import java.util.List;

public class CartFragment extends Fragment implements CartView{
    private CartAdapter cartAdapter;
    private CartPresenter presenter;
    private RecyclerView recyclerView;
    private Button confirmOrder;
    private CartListener mListener;
    private TextView price;
    private TextView empty;

    @Override
    public void setSum(double sum) {
        price.setText(String.format(getString(R.string.price), sum));
    }

    @Override
    public void setData(List<Food> data) {
        cartAdapter.setData(data);
    }

    @Override
    public void setOrderButtonVisibility(boolean visibility) {
        if(visibility) confirmOrder.setVisibility(View.VISIBLE);
        else confirmOrder.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyCartView() {
        empty.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideEmptyCartView() {
        empty.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new CartPresenter();
        presenter.bindView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);

        price = view.findViewById(R.id.price);
        empty = view.findViewById(R.id.empty_cart);
        confirmOrder = view.findViewById(R.id.btn_to_order);
        recyclerView = view.findViewById(R.id.list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        cartAdapter = new CartAdapter(getContext(), presenter, mListener);
        presenter.initCart();
        recyclerView.setAdapter(cartAdapter);

        CartItemTouchHelperCallback callback = new CartItemTouchHelperCallback(cartAdapter);

        ItemTouchHelper th = new ItemTouchHelper(callback);
        th.attachToRecyclerView(recyclerView);

        DefaultItemAnimator animator = new DefaultItemAnimator();
        recyclerView.setItemAnimator(animator);

        confirmOrder.setOnClickListener(view1 -> mListener.onConfirmOrderClick());

        return view;
    }

    private class CartItemTouchHelperCallback extends ItemTouchHelper.Callback{
        private final CartItemTouchHelperAdapter adapter;

        public CartItemTouchHelperCallback(CartItemTouchHelperAdapter adapter) {
            this.adapter = adapter;
        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            int dragFlags = 0;
            int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
            return makeMovementFlags(dragFlags, swipeFlags);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            adapter.onItemDismiss(viewHolder.getAdapterPosition());
        }

        @Override
        public boolean isItemViewSwipeEnabled() { return true; }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            Paint paint = new Paint();
            View view = viewHolder.itemView;
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_delete);
            int top = (view.getTop() + ((view.getTop() - view.getBottom()) / 2)) - (bitmap.getScaledHeight(c) / 2) - (view.getTop() - view.getBottom());
            if(dX > 0) c.drawBitmap(bitmap, 36, top, paint);
            else c.drawBitmap(bitmap, view.getRight() - 24 - bitmap.getWidth(), top, paint);
        }
    }

    public interface CartListener{
        void onConfirmOrderClick();
        void onCartItemClick(Food food);
    }

    @Override
    public void onResume() {
        super.onResume();
        cartAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CartListener) {
            mListener = (CartListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement CartListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
