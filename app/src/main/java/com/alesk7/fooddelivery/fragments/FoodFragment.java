package com.alesk7.fooddelivery.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.adapters.FoodAdapter;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.entities.FoodType;
import com.alesk7.fooddelivery.presenters.FoodPresenter;
import com.alesk7.fooddelivery.views.FoodView;

import java.util.Arrays;
import java.util.List;

public class FoodFragment extends Fragment implements FoodView {
    private FoodPresenter presenter;
    private OnFoodClickListener mListener;
    private FoodAdapter foodAdapter;
    private SwipeRefreshLayout refreshLayout;
    private TextView noConnection;
    private TextView mError;
    private RecyclerView list;

    @Override
    public void showNoConnectionView() {
        noConnection.setVisibility(View.VISIBLE);
        list.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideNoConnectionView() {
        noConnection.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorView() {
        mError.setVisibility(View.VISIBLE);
        list.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideErrorView() {
        mError.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingView() { refreshLayout.setRefreshing(true); }

    @Override
    public void hideLoadingView() { refreshLayout.setRefreshing(false); }

    @Override
    public void setData(List<Food> food) { foodAdapter.setData(food); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getContext() instanceof OnFoodClickListener) {
            mListener = (OnFoodClickListener) getContext();
        } else {
            throw new RuntimeException(getContext().toString() + " must implement OnFoodClickListener");
        }
        presenter = new FoodPresenter();
        presenter.bindView(this);
        presenter.init();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_food, container, false);

        final Spinner spinner = view.findViewById(R.id.spinner);
        final SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getContext(), R.layout.spinner_item, Arrays.asList(FoodType.values()));
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                presenter.loadData((FoodType)adapterView.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        refreshLayout = view.findViewById(R.id.swipeContainer);
        refreshLayout.setOnRefreshListener(() -> presenter.refreshData((FoodType)spinner.getSelectedItem()));

        noConnection = view.findViewById(R.id.no_connection);
        mError = view.findViewById(R.id.error);

        list = view.findViewById(R.id.list);
        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            list.setLayoutManager(new LinearLayoutManager(getContext()));
        } else if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            list.setLayoutManager(new GridLayoutManager(getContext(), 2));
        }

        foodAdapter = new FoodAdapter(getContext(), mListener, presenter);
        presenter.loadData((FoodType)spinner.getSelectedItem());
        list.setAdapter(foodAdapter);

        return view;
    }

    public interface OnFoodClickListener {
        void onFoodClick(Food item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener = null;
        presenter.destroy();
    }

    private class SpinnerAdapter extends ArrayAdapter<FoodType> {

        public SpinnerAdapter(Context context, int res, List<FoodType> data){
            super(context, res, data);
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent){
            view = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
            TextView textView = view.findViewById(R.id.spinner_item_text);
            textView.setText(getItem(position).title());
            return view;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            TextView view = (TextView) super.getDropDownView(position, convertView, parent);
            view.setText(getItem(position).title());
            return view;
        }
    }
}
