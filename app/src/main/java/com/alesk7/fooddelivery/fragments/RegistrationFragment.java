package com.alesk7.fooddelivery.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.presenters.RegisterPresenter;
import com.alesk7.fooddelivery.views.RegisterView;

public class RegistrationFragment extends Fragment implements RegisterView{
    private RegisterPresenter registerPresenter;
    private ProgressDialog progressDialog;
    private EditText mName;
    private EditText mLastName;
    private EditText mEmail;
    private EditText mPassword;

    @Override
    public String getName() {
        return mName.getText().toString();
    }

    @Override
    public String getLastName() {
        return mLastName.getText().toString();
    }

    @Override
    public String getEmail() {
        return mEmail.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPassword.getText().toString();
    }

    @Override
    public void hideRegisterScreen() {
        getActivity().finish();
    }

    @Override
    public void showLoadingIndicator() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Загрузка");
        progressDialog.setMessage("Идет регистрация");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoadingIndicator() {
        progressDialog.dismiss();
    }

    @Override
    public void showIncorrectDataMessage() {
        Toast.makeText(getContext(), "Неправильно заполнены поля!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(getContext(), "Ошибка", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmailNotUnique() {
        Toast.makeText(getContext(), "Пользователь с данным email уже зарегистрирован!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerPresenter = new RegisterPresenter();
        registerPresenter.bindView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        mName = view.findViewById(R.id.register_name);
        mLastName = view.findViewById(R.id.register_last_name);
        mEmail = view.findViewById(R.id.register_email);
        mPassword = view.findViewById(R.id.register_password);

        view.findViewById(R.id.register_button).setOnClickListener(view1 -> registerPresenter.tryRegisterClient());

        return view;
    }
}