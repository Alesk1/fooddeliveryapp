package com.alesk7.fooddelivery.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Acer on 27-Sep-17.
 */

public class Order {
    @SerializedName("OrderId")
    private int id;
    @SerializedName("ClientId")
    private int clientId;
    @SerializedName("AddressId")
    private int AddressId;
    @SerializedName("OrderTime")
    private long orderTime;
    @SerializedName("Cost")
    private float cost;
    @SerializedName("PhoneNumber")
    private String phoneNumber;
    @SerializedName("FoodList")
    private List<Food> food;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getAddressId() {
        return AddressId;
    }

    public void setAddressId(int addressId) {
        AddressId = addressId;
    }

    public long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(long orderTime) {
        this.orderTime = orderTime;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Food> getFood() {
        return food;
    }

    public void setFood(List<Food> food) {
        this.food = food;
    }
}
