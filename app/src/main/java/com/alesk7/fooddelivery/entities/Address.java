package com.alesk7.fooddelivery.entities;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Acer on 27-Sep-17.
 */

public class Address extends RealmObject{
    @SerializedName("AddressId")
    private int id;
    @SerializedName("ClientId")
    private int clientId;
    @SerializedName("AddressName")
    private String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return address;
    }
}
