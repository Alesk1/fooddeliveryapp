package com.alesk7.fooddelivery.entities;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Acer on 27-Sep-17.
 */

public class Food implements Serializable{
    @SerializedName("FoodId")
    private int id;
    @SerializedName("Type")
    private String type;
    private transient Bitmap image;
    @SerializedName("Image")
    private String imageUrl;
    @SerializedName("Name")
    private String name;
    @SerializedName("Description")
    private String description;
    @SerializedName("Size")
    private String size;
    @SerializedName("Weight")
    private int weight;
    @SerializedName("CookingTime")
    private int cookingTime;
    @SerializedName("Price")
    private float price;
    @SerializedName("Quantity")
    private int quantity;

    public int getQuantity() {return quantity;}
    public String getType() {
        return type.toString();
    }
    public Bitmap getBitmap() {
        return image;
    }
    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
    public String getSize() {
        return size;
    }
    public int getWeight() {
        return weight;
    }
    public int getCookingTime() {
        return cookingTime;
    }
    public float getPrice() {
        return price;
    }
    public int getId() { return id; }
    public String getImageUrl() { return imageUrl; }

    public void setQuantity(int quantity) { this.quantity = quantity; }
    public void setType(String type) { this.type = type; }
    public void setBitmapUrl(String url){
        this.imageUrl = url;
    }
    public void setBitmap(Bitmap image) {
        this.image = image;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setSize(String size) {
        this.size = size;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }
    public void setCookingTime(int cooking_time) {
        this.cookingTime = cooking_time;
    }
    public void setPrice(float price) {
        this.price = price;
    }
    public void setId(int id) {
        this.id = id;
    }
}
