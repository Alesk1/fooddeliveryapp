package com.alesk7.fooddelivery.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acer on 10-Nov-17.
 */

public class Cart {
    private static Cart instance;
    private List<Food> cartFood;
    private Double sum;

    private Cart(){
        cartFood = new ArrayList<>();
    }

    public static Cart getInstance(){
         return instance == null ? instance = new Cart() : instance;
    }

    public List<Food> getList() { return cartFood; }
    public void add(Food food){
        if(cartFood.contains(food)) cartFood.set(cartFood.indexOf(food), food);
        else cartFood.add(food);
    }
    public void remove(Food food){ cartFood.remove(food); }
    public Food get(int index){ return cartFood.get(index); }
    public Double getSum() { return sum; }
    public void setSum(Double sum) { this.sum = sum; }
}
