package com.alesk7.fooddelivery.entities;

/**
 * Created by Acer on 29-Sep-17.
 */

public enum FoodType {
    pizza {
        public String title(){return "Пиццы";}
    },

    burger {
        public String title(){return "Бургеры";}
    },

    cake {
        public String title(){return "Торты";}
    },

    hotDish {
        public String title(){return "Горячие блюда";}
    },

    salad {
        public String title(){return "Салаты";}
    };

    public abstract String title();
}
