package com.alesk7.fooddelivery.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.entities.Address;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.entities.Order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Acer on 26-Oct-17.
 */

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    private Context context;
    private List<Order> data = new ArrayList<>();

    public OrdersAdapter(Context context){
        this.context = context;
    }

    public void setData(List<Order> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_orders_item, parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = data.get(position);
        holder.address.setText(Realm.getDefaultInstance()
                .where(Address.class)
                .equalTo("id", holder.mItem.getAddressId())
                .findFirst()
                .getAddress());
        holder.orderTime.setText(new SimpleDateFormat(context.getString(R.string.dateFormat)).format(holder.mItem.getOrderTime()));
        holder.cost.setText(String.format(context.getString(R.string.price), holder.mItem.getCost()));
        OrderFoodAdapter adapter = new OrderFoodAdapter();
        adapter.setData(holder.mItem.getFood());
        holder.orderedFood.setLayoutManager(new LinearLayoutManager(context));
        holder.orderedFood.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        Order mItem;
        final TextView address;
        final TextView orderTime;
        final TextView cost;
        final RecyclerView orderedFood;

        ViewHolder(View view){
            super(view);
            address = view.findViewById(R.id.address);
            orderTime = view.findViewById(R.id.order_time);
            cost = view.findViewById(R.id.cost);
            orderedFood = view.findViewById(R.id.ordered_food);
        }
    }

    private class OrderFoodAdapter extends RecyclerView.Adapter<OrderFoodAdapter.ViewHolder>{
        List<Food> data;

        void setData(List<Food> data){
            this.data = data;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.mItem = data.get(position);
            holder.foodName.setText(holder.mItem.getName());
            holder.foodPrice.setText(String.format(context.getString(R.string.price), holder.mItem.getPrice() * holder.mItem.getQuantity()));
            holder.foodQuantity.setText(String.format(context.getString(R.string.quantity), holder.mItem.getQuantity()));
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            Food mItem;
            final TextView foodName;
            final TextView foodPrice;
            final TextView foodQuantity;

            ViewHolder(View view){
                super(view);
                foodName = view.findViewById(R.id.food_name);
                foodPrice = view.findViewById(R.id.food_price);
                foodQuantity = view.findViewById(R.id.food_quantity);
            }
        }
    }
}
