package com.alesk7.fooddelivery.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.fragments.CartFragment;
import com.alesk7.fooddelivery.presenters.CartPresenter;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> implements CartItemTouchHelperAdapter {
    private final Context context;
    private List<Food> data;
    private CartPresenter presenter;
    private CartFragment.CartListener mListener;

    public CartAdapter(Context context, CartPresenter presenter, CartFragment.CartListener listener){
        this.context = context;
        this.presenter = presenter;
        mListener = listener;
    }

    @Override
    public void onItemDismiss(int position) {
        if(position != RecyclerView.NO_POSITION) {
            data.remove(position);
            notifyItemRemoved(position);
            presenter.onItemRemoved();
        }
    }

    public void setData(List<Food> data){
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_cart_item, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        holder.btnClose.setOnClickListener(view1 -> onItemDismiss(holder.getAdapterPosition()));
        view.setOnClickListener((v) -> mListener.onCartItemClick(data.get(holder.getAdapterPosition())));
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = data.get(position);
        holder.foodPic.setImageBitmap(holder.mItem.getBitmap());
        holder.foodName.setText(holder.mItem.getName());
        holder.foodSize.setText(holder.mItem.getSize());
        holder.foodSum.setText(String.format(context.getString(R.string.price), (holder.mItem.getPrice() * holder.mItem.getQuantity())));
        holder.foodQuantity.setText(String.format(context.getString(R.string.quantity), holder.mItem.getQuantity()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        Food mItem;
        final View mView;
        final ImageView foodPic;
        final ImageButton btnClose;
        final TextView foodName;
        final TextView foodSum;
        final TextView foodSize;
        final TextView foodQuantity;

        ViewHolder(View view) {
            super(view);
            this.mView = view;
            this.foodPic = view.findViewById(R.id.food_pic);
            this.foodName = view.findViewById(R.id.food_name);
            this.foodSum = view.findViewById(R.id.food_sum);
            this.foodSize = view.findViewById(R.id.food_size);
            this.btnClose = view.findViewById(R.id.btn_close);
            this.foodQuantity = view.findViewById(R.id.quantity);
        }
    }
}
