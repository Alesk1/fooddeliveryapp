package com.alesk7.fooddelivery.adapters;

/**
 * Created by Acer on 04-Nov-17.
 */

public interface CartItemTouchHelperAdapter {
    void onItemDismiss(int position);
}
