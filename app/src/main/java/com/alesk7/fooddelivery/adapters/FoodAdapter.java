package com.alesk7.fooddelivery.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alesk7.fooddelivery.R;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.fragments.FoodFragment;
import com.alesk7.fooddelivery.presenters.FoodPresenter;

import java.util.ArrayList;
import java.util.List;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder> {
    private FoodPresenter presenter;
    private final Context context;
    private List<Food> data;
    private final FoodFragment.OnFoodClickListener mListener;

    public FoodAdapter(Context context, FoodFragment.OnFoodClickListener listener, FoodPresenter presenter) {
        this.context = context;
        mListener = listener;
        this.presenter = presenter;
        data = new ArrayList<>();
    }

    public void setData(List<Food> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_food_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = data.get(position);
        if (data.get(holder.getAdapterPosition()).getBitmap() != null) {
            holder.picture.setImageBitmap(holder.mItem.getBitmap());
        } else {
            presenter.loadFoodImage(data.get(holder.getAdapterPosition()), () -> notifyItemChanged(holder.getAdapterPosition()));
        }
        holder.name.setText(holder.mItem.getName());
        holder.cooking_time.setText(String.format(context.getString(R.string.minutes), holder.mItem.getCookingTime()));
        holder.size.setText(holder.mItem.getSize());
        holder.price.setText(String.format(context.getString(R.string.price), holder.mItem.getPrice()));

        holder.mView.setOnClickListener((v) -> mListener.onFoodClick(holder.mItem));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView picture;
        final TextView name;
        final TextView cooking_time;
        final TextView size;
        final TextView price;
        Food mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            picture = view.findViewById(R.id.concretefood_picture);
            name = view.findViewById(R.id.concretefood_name);
            cooking_time = view.findViewById(R.id.cooking_time);
            size = view.findViewById(R.id.concretefoodcard_size);
            price = view.findViewById(R.id.concretefoodcard_price);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + name.getText() + "'";
        }
    }
}
