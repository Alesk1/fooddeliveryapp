package com.alesk7.fooddelivery.presenters;

/**
 * Created by Acer on 27-Sep-17.
 */

public class BasePresenter<T> {
    T view;

    public void bindView(T view){
        this.view = view;
    }

    public void unBindView(){
        this.view = null;
    }
}
