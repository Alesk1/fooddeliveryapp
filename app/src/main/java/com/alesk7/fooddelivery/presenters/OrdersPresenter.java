package com.alesk7.fooddelivery.presenters;

import android.util.Log;

import com.alesk7.fooddelivery.entities.Client;
import com.alesk7.fooddelivery.repository.OrderRepository;
import com.alesk7.fooddelivery.views.OrdersView;

import java.util.Collections;

import io.realm.Realm;

/**
 * Created by Acer on 01-Nov-17.
 */

public class OrdersPresenter extends BasePresenter<OrdersView> {
    private OrderRepository orderRepository;
    private Realm realm;
    private Client client;

    public OrdersPresenter(){
        orderRepository = new OrderRepository();
        realm = Realm.getDefaultInstance();
    }

    public void onResume(){
        client = realm.where(Client.class).findFirst();
        if(client == null){
            view.showAuthorization();
        } else {
            view.hideAuthorization();
            loadOrders();
        }
    }

    public void loadOrders(){
        orderRepository.getAll(client.getId())
                .doOnSubscribe(s -> view.showLoadingIndicator())
                .doAfterTerminate(() -> view.hideLoadingIndicator())
                .subscribe(orders -> {
                                Collections.reverse(orders);
                                view.ordersDataLoaded(orders);
                            },
                           throwable -> {
                                Log.e(throwable.getLocalizedMessage(), throwable.getMessage());
                                view.hideLoadingIndicator();
                           });
    }
}
