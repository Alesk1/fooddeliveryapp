package com.alesk7.fooddelivery.presenters;

import com.alesk7.fooddelivery.entities.Cart;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.views.CartView;

/**
 * Created by Acer on 14-Oct-17.
 */

public class CartPresenter extends BasePresenter<CartView> {
    private Cart cart;

    public void initCart(){
        cart = Cart.getInstance();
        checkIsCartEmpty();
        view.setData(cart.getList());
        calculateSum();
        view.setSum(cart.getSum());
    }

    public void onItemRemoved(){
        calculateSum();
        view.setSum(cart.getSum());
        checkIsCartEmpty();
    }

    private void checkIsCartEmpty(){
        if(cart.getList().isEmpty()){
            view.setOrderButtonVisibility(false);
            view.showEmptyCartView();
        } else {
            view.setOrderButtonVisibility(true);
            view.hideEmptyCartView();
        }
    }

    private void calculateSum(){
        double sum = 0;
        for(Food f : cart.getList()){
            sum += f.getPrice() * f.getQuantity();
        }
        cart.setSum(sum);
    }

}
