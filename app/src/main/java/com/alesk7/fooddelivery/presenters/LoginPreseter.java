package com.alesk7.fooddelivery.presenters;

import android.util.Log;

import com.alesk7.fooddelivery.entities.Client;
import com.alesk7.fooddelivery.repository.AddressRepository;
import com.alesk7.fooddelivery.repository.ClientRepository;
import com.alesk7.fooddelivery.views.LoginView;

import java.util.List;

/**
 * Created by Acer on 27-Sep-17.
 */

public class LoginPreseter extends BasePresenter<LoginView> {
    private ClientRepository clientRepository;
    private AddressRepository addressRepository;

    public LoginPreseter(){
        clientRepository = new ClientRepository();
        addressRepository = new AddressRepository();
    }

    public void tryLogIn(){
        Client client = new Client();
        client.setEmail(view.getEmail());
        client.setPassword(view.getPassword());

        clientRepository.getAllClients()
                .doOnSubscribe(s -> view.showLoadingIndicator())
                .doAfterTerminate(() -> view.hideLoadingIndicator())
                .subscribe(clients -> {
                    Client cl = findClient(clients, client);
                    if(cl != null) logIn(cl);
                    else view.showIncorrectDataMessage();
                }, throwable -> {
                    Log.e(throwable.getLocalizedMessage(), throwable.getMessage());
                    view.showErrorMessage();
                    view.hideLoadingIndicator();
                });
    }

    private void logIn(Client client){
        clientRepository.createLocal(client);
        view.hideLogInScreen();
    }

    private Client findClient(List<Client> allClients, Client client){
        for(Client c : allClients){
            if(c.getEmail() != null && c.getPassword() != null)
                if(c.getEmail().equals(client.getEmail()) && c.getPassword().equals(client.getPassword()))
                    return c;
        }

        return null;
    }
}
