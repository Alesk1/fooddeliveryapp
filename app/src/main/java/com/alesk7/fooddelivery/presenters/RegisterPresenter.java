package com.alesk7.fooddelivery.presenters;

import android.util.Log;

import com.alesk7.fooddelivery.entities.Client;
import com.alesk7.fooddelivery.repository.ClientRepository;
import com.alesk7.fooddelivery.views.RegisterView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acer on 27-Sep-17.
 */

public class RegisterPresenter extends BasePresenter<RegisterView> {
    private ClientRepository clientRepository;

    public RegisterPresenter(){
        clientRepository = new ClientRepository();
    }

    public void tryRegisterClient(){
        Client client = new Client();
        client.setFirstName(view.getName());
        client.setLastName(view.getLastName());
        client.setEmail(view.getEmail());
        client.setPassword(view.getPassword());
        client.setBonusCash(1);

        if(!checkForCredentialsAccuracy(client)){
            view.showIncorrectDataMessage();
            return;
        }

        List<Client> allClients = new ArrayList<>();

        clientRepository.getAllClients()
                .doOnSubscribe(s -> view.showLoadingIndicator())
                .doAfterTerminate(() -> { if(checkIfEmailAvailable(allClients, client.getEmail())) registerClient(client); })
                .doAfterTerminate(() -> { if(!checkIfEmailAvailable(allClients, client.getEmail())) view.showEmailNotUnique();
                                            view.hideLoadingIndicator();} )
                .subscribe(allClients::addAll, throwable -> Log.e(throwable.getLocalizedMessage(), throwable.getMessage()));
    }

    private void registerClient(Client client){
        clientRepository.create(client)
                .doAfterTerminate(() -> view.hideLoadingIndicator())
                .doAfterTerminate(() -> view.hideRegisterScreen())
                .subscribe((r) -> {
                    client.setId(r.body());
                    clientRepository.createLocal(client);
                }, throwable -> {
                    Log.e(throwable.getLocalizedMessage(), throwable.getMessage());
                    view.hideLoadingIndicator();
                    view.showErrorMessage();
                });
    }

    private boolean checkForCredentialsAccuracy(Client client){
        if(client.getEmail().isEmpty() | client.getPassword().isEmpty()) return false;
        if(client.getPassword().length() < 6) return false;
        if(!client.getEmail().contains("@")) return false;
        return true;
    }

    private boolean checkIfEmailAvailable(List<Client> clients, String email){
        for(Client cl : clients) {
            if (cl.getEmail().equals(email)) return false;
        }

        return true;
    }
}
