package com.alesk7.fooddelivery.presenters;

import com.alesk7.fooddelivery.entities.Cart;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.views.FoodDescriptionView;

/**
 * Created by Acer on 06-Oct-17.
 */

public class FoodDescriptionPresenter extends BasePresenter<FoodDescriptionView> {
    private Food food;

    public void onCreate(Food food){
        this.food = food;
        view.setFoodName(food.getName());
        view.setFoodPicture(food.getBitmap());
        view.setFoodSize(food.getSize());
        view.setFoodWeight(food.getWeight());
        view.setFoodDescription(food.getDescription());
        view.setCookingTime(food.getCookingTime());
        view.setPrice(food.getPrice());
        if(food.getQuantity() > 0 && food.getQuantity() <= 100) {
            view.setQuantuty(food.getQuantity());
        }
    }

    public void onBtnCartClick(){
        food.setQuantity(view.getQuantity());
        Cart.getInstance().add(food);
        view.showAddedToCartMessage();
    }

    public void onChangeCount(int count){
        view.setPrice(food.getPrice() * count);
    }
}
