package com.alesk7.fooddelivery.presenters;

import android.util.Log;

import com.alesk7.fooddelivery.entities.Address;
import com.alesk7.fooddelivery.entities.Client;
import com.alesk7.fooddelivery.repository.AddressRepository;
import com.alesk7.fooddelivery.repository.ClientRepository;
import com.alesk7.fooddelivery.views.ClientView;

import io.realm.Realm;

/**
 * Created by Acer on 03-Nov-17.
 */

public class ClientPresenter extends BasePresenter<ClientView> {
    private ClientRepository clientRepository;
    private AddressRepository addressRepository;
    private Client client;

    public ClientPresenter(){
        clientRepository = new ClientRepository();
        addressRepository = new AddressRepository();
    }

    public void onResume(){
        client = ClientRepository.get();
        if(client != null){
            view.hideAuthorizationView();
            view.setName(client.getFirstName() + " " + client.getLastName());
            view.setEmail(client.getEmail());
            view.setBonusCash(client.getBonusCash());
            view.setAddresses(addressRepository.getAllClientAddresses(client.getId()));
        } else {
            view.showAuthorizationView();
        }
    }

    public void logOut(){
        clientRepository.clearLocal();
        addressRepository.clearLocal();
        view.showAuthorizationView();
    }

    public void addAddress(String address){
        Address addressObject = new Address();
        addressObject.setClientId(Realm.getDefaultInstance().where(Client.class).findFirst().getId());
        addressObject.setAddress(address);
        addressRepository.create(addressObject)
                .doOnSubscribe(s -> view.showLoadingIndicator())
                .doAfterTerminate(() -> view.hideLoadingIndicator())
                .subscribe((r) -> addressObject.setId(r.body()), throwable -> {
                    Log.e(throwable.getLocalizedMessage(), throwable.getMessage());
                    view.hideLoadingIndicator();
                });
    }

    public void deleteAddress(Address address){
        addressRepository.delete(address)
                .doOnSubscribe(s -> view.showLoadingIndicator())
                .doAfterTerminate(() -> view.hideLoadingIndicator())
                .subscribe(response -> {}, throwable -> Log.e(throwable.getLocalizedMessage(), throwable.getMessage()));
    }

    public void editAddress(Address address, String newAddress){
        addressRepository.update(address, newAddress)
                .doOnSubscribe(s -> view.showLoadingIndicator())
                .doAfterTerminate(() -> view.hideLoadingIndicator())
                .subscribe(response -> {}, throwable -> Log.e(throwable.getLocalizedMessage(), throwable.getMessage()));
    }

}
