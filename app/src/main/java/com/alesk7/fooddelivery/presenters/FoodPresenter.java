package com.alesk7.fooddelivery.presenters;

import android.graphics.BitmapFactory;
import android.util.Log;

import com.alesk7.fooddelivery.FoodDeliveryApplication;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.entities.FoodType;
import com.alesk7.fooddelivery.repository.FoodRepository;
import com.alesk7.fooddelivery.views.FoodView;
import com.alesk7.fooddelivery.views.callbacks.FoodImageLoadedCallback;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FoodPresenter extends BasePresenter<FoodView> {
    private FoodRepository foodRepository;
    private Observable<List<Food>> observable;
    private Disposable disposable;
    private FoodType currentFoodType;

    public void loadData(FoodType type){
        if(!FoodDeliveryApplication.isConnectedToNetwork()){
            view.showNoConnectionView();
            return;
        } else {
            view.hideNoConnectionView();
        }

        if(observable == null || type != currentFoodType) {
            observable = foodRepository.get(type)
                    .doOnSubscribe(s -> view.showLoadingView())
                    .doAfterTerminate(() -> view.hideLoadingView())
                    .cache();
            currentFoodType = type;
        }

        disposable = observable.subscribe(food -> {
            view.setData(food);
            view.hideErrorView();
        }, throwable -> view.showErrorView());
    }

    public void refreshData(FoodType type){
        observable = null;
        loadData(type);
    }

    public void init(){
        foodRepository = new FoodRepository();
    }

    public void destroy(){
        unBindView();
        if(disposable != null) disposable.dispose();
    }

    public void loadFoodImage(Food food, FoodImageLoadedCallback callback){
        Flowable.fromArray(food.getImageUrl())
                .flatMap(url -> {
                        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
                        connection.connect();
                    return Flowable.fromArray(BitmapFactory.decodeStream(connection.getInputStream()));
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bitmap -> {
                            food.setBitmap(bitmap);
                            callback.notifyImageLoaded();
                        }, throwable -> Log.e("IMAGE ERROR", throwable.getMessage()));
    }

}
