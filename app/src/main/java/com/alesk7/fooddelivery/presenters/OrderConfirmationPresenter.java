package com.alesk7.fooddelivery.presenters;

import android.util.Log;

import com.alesk7.fooddelivery.entities.Address;
import com.alesk7.fooddelivery.entities.Cart;
import com.alesk7.fooddelivery.entities.Client;
import com.alesk7.fooddelivery.entities.Order;
import com.alesk7.fooddelivery.repository.AddressRepository;
import com.alesk7.fooddelivery.repository.ClientRepository;
import com.alesk7.fooddelivery.repository.OrderRepository;
import com.alesk7.fooddelivery.views.OrderConfirmationView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Acer on 11-Dec-17.
 */

public class OrderConfirmationPresenter extends BasePresenter<OrderConfirmationView> {
    private Cart cart;
    private ClientRepository clientRepository;
    private AddressRepository addressRepository;
    private OrderRepository orderRepository;
    private Client client;
    private float discount;

    public OrderConfirmationPresenter(){
        cart = Cart.getInstance();
        orderRepository = new OrderRepository();
        clientRepository = new ClientRepository();
        addressRepository = new AddressRepository();
    }

    public void onResume(){
        client = ClientRepository.get();

        if(client == null) {
            view.setAuthVisible(true);
        }else{
            view.setAuthVisible(false);
            view.setAddressesList(Realm.getDefaultInstance().where(Address.class).findAll());
            view.setAvailableDiscount(client.getBonusCash());
            view.setName(client.getFirstName());
            view.setCost(cart.getSum().floatValue());
        }
    }

    public void onUseDiscountChecked(boolean checked){
        if(checked){
            view.setDiscountPercentageVisible(true);
            double percentage = 0;
            if(client.getBonusCash() <= cart.getSum().floatValue()) {
                discount = client.getBonusCash();
                percentage = client.getBonusCash() / (Cart.getInstance().getSum() / 100);
            } else {
                percentage = 100;
                discount = cart.getSum().floatValue();
            }

            view.setCost(cart.getSum().floatValue() - discount);
            view.setDiscountPercentage((float) percentage);
        }else{
            view.setDiscountPercentageVisible(false);
            view.setCost(Cart.getInstance().getSum().floatValue());
            discount = 0;
        }
    }

    public void addAddress(String address){
        Address addressObject = new Address();
        addressObject.setClientId(Realm.getDefaultInstance().where(Client.class).findFirst().getId());
        addressObject.setAddress(address);
        addressRepository.create(addressObject)
                .doOnSubscribe(s -> view.showLoadingView())
                .doAfterTerminate(() -> view.hideLoadingView())
                .doAfterTerminate(() -> view.setAddressesList(Realm.getDefaultInstance().where(Address.class).findAll()))
                .subscribe((r) -> addressObject.setId(r.body()), throwable -> {
                    Log.e(throwable.getLocalizedMessage(), throwable.getMessage());
                    view.hideLoadingView();
                });
    }

    public void trySendOrder(){
        if(view.getAddress() == null | view.getPhoneNumber().equals("") | view.getName().equals("")){
            view.showFillFieldsMessage();
            return;
        }

        List<Client> allClients = new ArrayList<>();

        clientRepository.getAllClients()
                .doOnSubscribe((s) -> view.showLoadingView())
                .doAfterTerminate(() -> { if(checkIfClientValid(allClients)) sendOrder(); })
                .doAfterTerminate(() -> { if(!checkIfClientValid(allClients)) logOutClient(); })
                .subscribe(allClients::addAll, throwable -> {
                    Log.e(throwable.getLocalizedMessage(), throwable.getMessage());
                    view.hideLoadingView();
                });
    }

    private void sendOrder(){
        Order order = new Order();
        order.setClientId(client.getId());
        order.setAddressId(view.getAddress().getId());
        order.setOrderTime(new Date().getTime());
        order.setCost(cart.getSum().floatValue());
        order.setPhoneNumber(view.getPhoneNumber());
        order.setFood(cart.getList());

        Client client = new Client(this.client);

        if(discount > 0){
            cart.setSum(cart.getSum() - discount);
            client.setBonusCash(client.getBonusCash() - discount);
        }else{
            client.setBonusCash(client.getBonusCash() + (cart.getSum().floatValue() * 0.1f));
        }

        orderRepository.create(order)
                .doAfterTerminate(() -> view.hideLoadingView())
                .subscribe(response -> {
                    updateClient(client);
                    view.showSuccessSendingView();
                    view.setAvailableDiscount(client.getBonusCash());
                }, throwable -> Log.e(throwable.getLocalizedMessage(), throwable.getMessage()));
    }

    private void logOutClient(){
        clientRepository.clearLocal();
        addressRepository.clearLocal();
        view.hideLoadingView();
        view.showClientError();
        view.setAuthVisible(true);
    }

    private void updateClient(Client client){
        clientRepository.update(client)
                .subscribe(r -> {}, throwable -> Log.e(throwable.getLocalizedMessage(), throwable.getMessage()));
    }

    private boolean checkIfClientValid(List<Client> clients){
        for(Client c : clients){
            if(c.getEmail().equals(client.getEmail()) && c.getPassword().equals(client.getPassword())) return true;
        }

        return false;
    }
}
