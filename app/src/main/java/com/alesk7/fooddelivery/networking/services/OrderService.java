package com.alesk7.fooddelivery.networking.services;

import com.alesk7.fooddelivery.entities.Order;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Acer on 19-Dec-17.
 */

public interface OrderService {
    @POST("order/add")
    Observable<Response<Void>> addOrder(@Body Order body);

    @GET("order/get/{clientId}")
    Observable<List<Order>> getOrders(@Path("clientId") int clientId);
}
