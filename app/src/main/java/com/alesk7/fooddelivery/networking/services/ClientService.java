package com.alesk7.fooddelivery.networking.services;

import com.alesk7.fooddelivery.entities.Client;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by Acer on 12-Dec-17.
 */

public interface ClientService {
    @POST("client/add")
    @FormUrlEncoded
    Observable<Response<Integer>> addClient(@Field("FirstName") String firstName,
                                            @Field("LastName") String lastName,
                                            @Field("Email") String email,
                                            @Field("Password") String password,
                                            @Field("BonusCash") float bonusCash);

    @PUT("client/edit")
    @FormUrlEncoded
    Observable<Response<Void>> updateClient(@Field("ClientId") int id,
                                            @Field("FirstName") String firstName,
                                            @Field("LastName") String lastName,
                                            @Field("Email") String email,
                                            @Field("Password") String password,
                                            @Field("BonusCash") float bonusCash);

    @GET("client/get")
    Observable<List<Client>> getClients();
}
