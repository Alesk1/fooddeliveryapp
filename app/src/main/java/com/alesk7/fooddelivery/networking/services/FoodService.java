package com.alesk7.fooddelivery.networking.services;

import com.alesk7.fooddelivery.entities.Food;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Acer on 18-Nov-17.
 */

public interface FoodService {
    @GET("food/get/{foodType}")
    Observable<List<Food>> getFood(@Path("foodType") String foodType);
}
