package com.alesk7.fooddelivery.networking;

import com.alesk7.fooddelivery.BuildConfig;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Acer on 22-Nov-17.
 */

public class RetrofitFactory {

    public static Retrofit build(){
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().disableHtmlEscaping().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

}
