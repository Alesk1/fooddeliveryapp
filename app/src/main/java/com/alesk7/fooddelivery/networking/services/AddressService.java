package com.alesk7.fooddelivery.networking.services;

import com.alesk7.fooddelivery.entities.Address;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Acer on 17-Dec-17.
 */

public interface AddressService {
    @POST("address/add")
    @FormUrlEncoded
    Observable<Response<Integer>> addAddress(@Field("ClientId") int clientId,
                                             @Field("AddressName") String address);

    @PUT("address/edit")
    @FormUrlEncoded
    Observable<Response<Void>> updateAddress(@Field("AddressId") int id,
                                             @Field("ClientId") int clientId,
                                             @Field("AddressName") String address);

    @HTTP(method = "DELETE", path = "address/delete", hasBody = true)
    @FormUrlEncoded
    Observable<Response<Void>> deleteAddress(@Field("AddressId") int id,
                                             @Field("ClientId") int clientId,
                                             @Field("AddressName") String address);

    @GET("address/get/client/{clientId}")
    Observable<List<Address>> getClientAddresses(@Path("clientId") String clientId);
}
