package com.alesk7.fooddelivery;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.widget.EditText;

import com.alesk7.fooddelivery.entities.Address;
import com.alesk7.fooddelivery.entities.Food;
import com.alesk7.fooddelivery.fragments.CartFragment;
import com.alesk7.fooddelivery.fragments.ClientFragment;
import com.alesk7.fooddelivery.fragments.FoodDescriptionFragment;
import com.alesk7.fooddelivery.fragments.FoodFragment;
import com.alesk7.fooddelivery.fragments.OrderConfirmationFragment;
import com.alesk7.fooddelivery.fragments.OrdersFragment;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity implements FoodFragment.OnFoodClickListener, CartFragment.CartListener,
        OrderConfirmationFragment.OrderConfirmationListener, ClientFragment.ClientListener, OrdersFragment.OrdersListener,
        FoodDescriptionFragment.CartClickListener{
    private static Fragment foodFragment;
    private static CartFragment cartFragment;
    private static OrdersFragment ordersFragment;
    private static ClientFragment clientFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Realm.init(getBaseContext());
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(configuration);

        setContentView(R.layout.activity_main);
        final FragmentManager manager = getSupportFragmentManager();

        if(savedInstanceState == null){
            foodFragment = new FoodFragment();
            manager.beginTransaction().replace(R.id.main_container, foodFragment).commit();
        }

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch(item.getItemId()){
                case R.id.nav_food:{
                    if(foodFragment == null) foodFragment = new FoodFragment();
                    manager.beginTransaction().replace(R.id.main_container, foodFragment).commit();
                    break;
                }
                case R.id.nav_cart:{
                    if(cartFragment == null) cartFragment = new CartFragment();
                    manager.beginTransaction().replace(R.id.main_container, cartFragment).commit();
                    break;
                }
                case R.id.nav_history:{
                    if(ordersFragment == null) ordersFragment = new OrdersFragment();
                    manager.beginTransaction().replace(R.id.main_container, ordersFragment).commit();
                    break;
                }
                case R.id.nav_user_account:{
                    if(clientFragment == null) clientFragment = new ClientFragment();
                    manager.beginTransaction().replace(R.id.main_container, clientFragment).commit();
                    break;
                }
            }

            item.setChecked(true);
            return false;
        });
    }

    public void showAddressDialog(Address address, boolean add, AddressAddedCallback added, AddressEditedCallback edited) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        EditText mAddressText = new EditText(new ContextThemeWrapper(this, R.style.EditTextTheme));
        if(!add) mAddressText.setText(address.getAddress());
        builder.setTitle(add ? "Добавить адрес" : "Редактировать адрес")
                .setView(mAddressText)
                .setCancelable(true)
                .setNegativeButton("Отмена", ((dialog, i) -> dialog.cancel()))
                .setPositiveButton(add ? "Добавить" : "Сохранить",
                        add ? (dialog, id) -> added.added(mAddressText.getText().toString())
                                : (dialog, id) -> edited.edited(address, mAddressText.getText().toString()));
        AlertDialog alert = builder.create();
        alert.show();
    }

    public interface AddressAddedCallback{
        void added(String address);
    }

    public interface AddressEditedCallback{
        void edited(Address oldAddress, String newAddress);
    }

    private void startFragment(Fragment fragment, Bundle bundle, boolean addToBackStack){
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_container, fragment).commit();
        if(addToBackStack) transaction.addToBackStack(null);
    }

    @Override
    public void onFoodClick(Food item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.food_item), item);
        startFragment(new FoodDescriptionFragment(), bundle, true);
    }

    @Override
    public void onConfirmOrderClick() {
        startFragment(new OrderConfirmationFragment(), null, true);
    }

    @Override
    public void onCartItemClick(Food food) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.food_item), food);
        startFragment(new FoodDescriptionFragment(), bundle, true);
    }

    @Override
    public void onCartClick() {
        startFragment(new CartFragment(), null, true);
    }

    @Override
    public void onAuthorizationClick() {
        startActivity(new Intent(this, LoginActivity.class));
    }
}
